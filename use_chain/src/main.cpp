#ifndef _MSC_VER
#include "stdafx.h"
#endif

#include <fstream>
#include <string>

#include <cereal/types/unordered_map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/archives/binary.hpp>

#include <getopt.h>

#include "util.hpp"
#include "markovchain.hpp"


///
/// It reads serialized markov chain from a file,
/// and uses it to create a sequence of words.
///
int main(int argc, char** argv) {
  using namespace markov;

  // Parse the command line arguments with help of getopt.
  int opt;

  std::string test_data_file_name = "";
  std::string markov_chain_file_name = "";
  size_t n = 0;
  bool is_print_additional_info = false;

  std::string help_message = "\n"
                             "-t test_data.txt -m markov_chain.txt -n 2\n"
                             "\n"
                             "-t - specified a test data file name\n"
                             "-m - specified a file name for serialized markov chain\n"
                             "-n - specified the number of words we need to generate "
                             "to continue the sequence of words from the test data file\n"
                             "-p - output additional information\n"
                             "-h - help";

  while ((opt = getopt(argc, argv, "hpt:m:n:")) != EOF) {

    switch(opt) {
      case 't':
        test_data_file_name = optarg;
        break;
      case 'm':
        markov_chain_file_name = optarg;
        break;
      case 'n':
        n = std::stoul(optarg);
        break;
      case 'p':
        is_print_additional_info = true;
        break;
      case 'h':
        std::cout << help_message << std::endl;
        exit(0);
      default:
        std::cout << help_message << std::endl;
        exit(0);
    }
  }

  if (test_data_file_name == "") {
    std::cout << "test data file name is not specified!" << std::endl;
    std::cout << help_message << std::endl;
    exit(0);
  }

  if (markov_chain_file_name == "") {
    std::cout << "markov chain file name is not specified!" << std::endl;
    std::cout << help_message << std::endl;
    exit(0);
  }

  if (n == 0) {
    std::cout << "the number of words we need to generate "
                 "to continue the sequence of words from "
                 "the test data file is not specified!" << std::endl;
    std::cout << help_message << std::endl;
    exit(0);
  }

  // Output the command line arguments.
  if (is_print_additional_info) {
    std::cout << "test data file name: "
              << test_data_file_name << std::endl;

    std::cout << "markov chain file name: "
              << markov_chain_file_name << std::endl;

    std::cout << "number of words we need to generate: "
              << n << std::endl;
  }

  // Deserialize a markov chain.
  std::ifstream is(markov_chain_file_name, std::ios::in);

  MarkovChain markov_chain;

  if (is.is_open() == true) {
    cereal::BinaryInputArchive iarchive(is);

    try {
      iarchive >> markov_chain;
    } catch (const cereal::Exception & ex) {
      std::cerr << "Can't deserialize a data from a file. File name: "
                << markov_chain_file_name
                << std::endl;

      exit(1);
    }

    if (is_print_additional_info) {
      std::cout << markov_chain;
    }

    is.close();
  } else {
    // throw an error!
    std::cerr << "Can't open a file for input! File name: "
              << markov_chain_file_name
              << std::endl;

    exit(1);
  }

  // Read an input file.
  std::vector<std::string> test_data = Util::read_to_vector(
        test_data_file_name);

  // Check if test data is empty.
  if (test_data.empty()) {
    std::cerr << "Test data from a file is empty! File name: "
              << test_data_file_name
              << std::endl;

    exit(1);
  }

  // Create a start element based on the order of a markov chain.
  std::stringstream ss_start_element;
  std::string start_element;
  size_t order = markov_chain.order();
  size_t test_data_size = test_data.size();

  // If the order = 1 then take the last element in an array.
  //
  // Otherwise take the number of elements from the end of the array
  // and merge them together.
  // The number of elements is equal to the order of a markov chain.
  if (order > 1) {
    for (size_t i = (test_data_size - order); i < test_data_size; i++) {
      ss_start_element << test_data[i];

      // Exclude the last element.
      if (i < test_data_size - 1) {
        ss_start_element << " ";
      }
    }

    start_element = ss_start_element.str();
  } else {
    start_element = test_data.back();
  }

  if (is_print_additional_info) {
    std::cout << "start_element: " << start_element << std::endl;
  }

  // Set current item equals to the last item in the test data.
  // It works with first order markov chain.
  // It won't work for high order markov chain.
  markov_chain.set_current_item(start_element);

  // Continue a sequence from the test data
  // and output the result to the screen.
  while ((n > 0) && markov_chain.move_next()) {
    std::cout << markov_chain.current_item() << " " << std::endl;
    n--;
  }

  std::cout << "Program has finished the execution!" << std::endl;

  return 0;
}
