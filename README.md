# README #

The project creates and uses Markov chain to generate new sequences of words.

More information about Markov chain is provided by the following links:
[wikipedia](https://en.wikipedia.org/wiki/Markov_chain), [udacity](https://classroom.udacity.com/courses/cs271/lessons/48734405/concepts/487221790923#), [Markov Chains
Explained Visually](http://setosa.io/ev/markov-chains/).

### How do I get set up? ###

* OS: Ubuntu 14.04 x64, Ubuntu 16.04 x64.
* Compiler: gcc 4.8.4 and higher.
* Dependencies: [cmake](https://cmake.org/), [googletest](https://github.com/google/googletest), [cereal](https://github.com/USCiLab/cereal.git).
* How to run tests: all tests are located in the tests folder.

### Contribution ###

The project uses [google style](https://google.github.io/styleguide/cppguide.html).

### How to compile the project ###

```
#!bash

mkdir build
cd build
cmake ..
make
```

### How to run the project ###

To run the project open the *bin* folder and run application.
There are two main applications.

The first one is called *make_chain*. 
It reads input data from a file, creates a markov chain and serializes it to a file

The second one is called *use_chain*.
It reads serialized markov chain from a file, and uses it to create a sequence of words.

### An example of usage ###

To create a markov chain you have to pass a *training data*, *serialized markov chain* file names and the order of a markov chain as an arguments.

For example:




```
#!bash

# For first order markov chain:
-t test_data.txt -m markov_chain.txt -o 1

# For second order markov chain:
-t test_data.txt -m markov_chain.txt -o 2

# For help:
-h
```

*training_data.txt* has to contain a *number of rows* and a *sequence of words*.

For example:

```
#!

10
A
B
A
A
B
C
D
C
A
D
```


To use previously created markov chain you have to pass a test data, serialized markov chain file names and number of words you want to generate as an arguments.

For example:

```
#!bash

-t test_data.txt -m markov_chain.txt -n 2

# For help:
-h
```


*test_data.txt* has to contain a *number of rows* and a *sequence of words*.

For example:

```
#!

10
A
B
A
A
B
C
D
C
A
D
```

**Notice**: 
You could also download training_data.txt, test_data.txt and test_input_read_to_vector_1.txt (it is used in a unittest) in the downloads section as a zip file. After downloading the zip file unpack it in the bin project folder.

*first_order_markov_chain.zip* - it contains *training_data.txt*, *test_data.txt*, *markov_chain.txt* for first order markov chain.

*high_order_markov_chain.zip* - it contains *training_data.txt*, *test_data.txt*, *markov_chain.txt* for second order markov chain.

*tests_data.zip* - it contains *test_input_read_to_vector_1.txt*.