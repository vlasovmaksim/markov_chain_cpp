#pragma once

#include <vector>
#include <string>
#include <iostream>

namespace markov {

class Util {
public:
  Util() = delete;
  Util(const Util &) = delete;
  Util & operator = (const Util &) = delete;
  Util(const Util &&) = delete;
  Util & operator = (const Util &&) = delete;

  static std::vector<std::string> read_to_vector(
      const std::string & file_name);
};

} // markov
