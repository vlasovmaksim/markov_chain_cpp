#pragma once

#ifndef _MSC_VER
#include "stdafx.h"
#endif

#include <vector>
#include <string>
#include <unordered_map>
#include <iostream>

namespace markov {

class MarkovChain {
public:
  /// order is an order of a Markov chain.
  MarkovChain(const size_t & order = 1);

  MarkovChain(const MarkovChain &) = delete;
  MarkovChain & operator = (const MarkovChain &) = delete;
  MarkovChain(const MarkovChain &&) = delete;
  MarkovChain & operator = (const MarkovChain &&) = delete;

  /// It  constructs Markov matrix.
  void parse(std::vector<std::string> input_data);

  /// It returns a row from a matrix.
  const std::unordered_map<std::string, size_t> & row(
      const std::string & row_index) const;

  /// It returns a value from a matrix.
  const size_t & probability(const std::string & row_index,
                               const std::string & col_index) const;

  /// Set current item.
  void set_current_item(const std::string & item);

  /// Returns a row index.
  std::string current_item() const;

  /// Returns the order of a markov chain.
  size_t order() const;

  /// Move next owerwise return false.
  bool move_next();

  template<class Archive>
  void serialize(Archive & archive) {
    archive(data_);
    archive(order_);
  }

  friend std::ostream & operator << (std::ostream & os,
                                     const MarkovChain & rhs);

private:
  std::unordered_map<std::string, std::unordered_map<std::string, size_t>> data_;

  std::string current_item_ = "";

  size_t order_ = 1;
};

std::ostream & operator << (std::ostream & os,
                            const MarkovChain & rhs) {
  for (const auto & row : rhs.data_) {
    os << row.first << ": ";

    for (const auto & column : row.second) {
      os << column.first << " ";
    }

    os << std::endl;
  }
}

} // markov
