#include "util.hpp"
#include <fstream>


using namespace markov;

std::vector<std::string> Util::read_to_vector(
    const std::string& file_name) {
  std::ifstream ifs(file_name);

  std::vector<std::string> output_data;

  if (ifs.is_open() == true) {
    size_t n = 0;

    ifs >> n;

    output_data.reserve(n);

    for (size_t i = 0; i < n; i++) {
      std::string tmp;

      ifs >> tmp;

      output_data.push_back(tmp);
    }

    ifs.close();
  } else {
    std::cerr << "Can't open a file for reading! File name"
              << file_name
              << std::endl;
  }

  return output_data;
}
