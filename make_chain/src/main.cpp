#ifndef _MSC_VER
#include "stdafx.h"
#endif

#include <fstream>

#include <cereal/types/unordered_map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/archives/binary.hpp>

#include <getopt.h>

#include "util.hpp"
#include "markovchain.hpp"


///
/// It reads input data from a file,
/// creates a markov chain and serializes it to a file.
///
int main(int argc, char** argv) {
  using namespace markov;

  // Parse the command line arguments with help of getopt.
  int opt;

  std::string training_data_file_name = "";
  std::string markov_chain_file_name = "";
  size_t order = 0;
  bool is_print_additional_info = false;

  std::string help_message = "\n"
                             "-t test_data.txt -m markov_chain.txt -o 2\n"
                             "\n"
                             "-t - specified a training data file name\n"
                             "-m - specified a file name for serialized markov chain\n"
                             "-o - specified the order of a markov chain\n"
                             "-p - output additional information\n"
                             "-h - help";

  while ((opt = getopt(argc, argv, "hpt:m:o:")) != EOF) {

    switch(opt) {
      case 't':
        training_data_file_name = optarg;
        break;
      case 'm':
        markov_chain_file_name = optarg;
        break;
      case 'o':
        order = std::stoul(optarg);
        break;
      case 'p':
        is_print_additional_info = true;
        break;
      case 'h':
        std::cout << help_message << std::endl;
        exit(0);
      default:
        std::cout << help_message << std::endl;
        exit(0);
    }
  }

  if (training_data_file_name == "") {
    std::cout << "training data file name is not specified!" << std::endl;
    std::cout << help_message << std::endl;
    exit(0);
  }

  if (markov_chain_file_name == "") {
    std::cout << "markov chain file name is not specified!" << std::endl;
    std::cout << help_message << std::endl;
    exit(0);
  }

  if (order == 0) {
    std::cout << "the order of a markov chain is not specified!" << std::endl;
    std::cout << help_message << std::endl;
    exit(0);
  }

  // Output the command line arguments.
  if (is_print_additional_info) {
    std::cout << "training data file name: "
              << training_data_file_name << std::endl;

    std::cout << "markov chain file name: "
              << markov_chain_file_name << std::endl;

    std::cout << "order of markov chain: "
              << order << std::endl;
  }

  // Read an input file.
  std::vector<std::string> input_data = Util::read_to_vector(
        training_data_file_name);

  MarkovChain markov(order);

  // Create a markov chain.
  markov.parse(input_data);

  // Serialize the markov chain and store it in a file.
  std::ofstream os(markov_chain_file_name,
                   std::ios::out | std::ios::trunc);

  if (os.is_open() == true) {
    cereal::BinaryOutputArchive oarchive(os);

    try {
      oarchive << markov;
    } catch (const cereal::Exception & ex) {
      std::cerr << "Can't serialize a data to a file. File name: "
                << markov_chain_file_name
                << std::endl;

      exit(1);
    }

    os.close();
  } else {
    std::cerr << "Can't open a file for output! File name: "
              << markov_chain_file_name
              << std::endl;

    exit(1);
  }

  std::cout << "Program has finished the execution!" << std::endl;

  return 0;
}
