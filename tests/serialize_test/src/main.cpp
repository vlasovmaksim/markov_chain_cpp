#include "gtest/gtest.h"

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

#include <cereal/types/unordered_map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>

#include "markovchain.hpp"


using namespace markov;

TEST(GTEST, test_serialize_data_string) {
  std::string output_data = "ABC";
  std::string input_data = "";

  std::ostringstream os;

  cereal::BinaryOutputArchive oarchive(os);

  oarchive << output_data;

  std::istringstream is(os.str());

  cereal::BinaryInputArchive iarchive(is);

  iarchive >> input_data;

  EXPECT_EQ(output_data, input_data);
}

TEST(GTEST, test_serialize_unordered_map_1) {
  std::unordered_map<std::string, size_t> map = { { "A", 1 }, { "B", 2 } };

  EXPECT_EQ(map.at("A"), 1);
  EXPECT_EQ(map.at("B"), 2);

  std::ostringstream os;

  cereal::BinaryOutputArchive oarchive(os);

  oarchive << map;

  std::istringstream is(os.str());

  cereal::BinaryInputArchive iarchive(is);

  std::unordered_map<std::string, size_t> map_check;

  iarchive >> map_check;

  EXPECT_EQ(map.at("A"), map_check.at("A"));
}

TEST(GTEST, test_serialize_unordered_map_2) {
  std::unordered_map<std::string,
      std::unordered_map<std::string, size_t>> map;

  map["A"] = { { "A", 1 }, { "B", 1 } };
  map["B"] = { { "B", 1 } };

  EXPECT_EQ(map.at("A").at("A"), 1);
  EXPECT_EQ(map.at("A").at("B"), 1);
  EXPECT_EQ(map.at("B").at("B"), 1);

  std::ostringstream os;

  cereal::BinaryOutputArchive oarchive(os);

  oarchive << map;

  std::istringstream is(os.str());

  cereal::BinaryInputArchive iarchive(is);

  std::unordered_map<std::string,
      std::unordered_map<std::string, size_t>> map_check;

  iarchive >> map_check;

  EXPECT_EQ(map.at("A").at("A"), map_check.at("A").at("A"));
  EXPECT_EQ(map.at("A").at("B"), map_check.at("A").at("B"));
  EXPECT_EQ(map.at("B").at("B"), map_check.at("B").at("B"));
}

TEST(GTEST, test_serialize_data_string_to_file) {
  std::string output_data = "ABC";
  std::string input_data = "";

  {
    std::ofstream os("out.txt", std::ios::out | std::ios::trunc);

    if (os.is_open() == true) {
      cereal::BinaryOutputArchive oarchive(os);

      oarchive << output_data;

      os.close();
    } else {
      std::cerr << "Can't open file for output!" << std::endl;
    }
  }

  {
    std::ifstream is("out.txt", std::ios::in);

    if (is.is_open() == true) {
      cereal::BinaryInputArchive iarchive(is);

      iarchive >> input_data;

      is.close();
    } else {
      std::cerr << "Can't open file for input!" << std::endl;
    }
  }

  EXPECT_EQ(output_data, input_data);
}

TEST(GTEST, test_serialize_markov_chain_1) {
  std::vector<std::string> input_data = { "A", "B", "A", "A" };

  MarkovChain markov;

  markov.parse(input_data);

  std::ostringstream os;

  cereal::BinaryOutputArchive oarchive(os);

  oarchive << markov;
}

TEST(GTEST, test_serialize_markov_chain_2) {
  std::vector<std::string> input_data = { "A", "B", "A", "A" };

  MarkovChain markov;

  markov.parse(input_data);

  std::ostringstream os;

  cereal::BinaryOutputArchive oarchive(os);

  oarchive << markov;

  std::istringstream is(os.str());

  cereal::BinaryInputArchive iarchive(is);

  MarkovChain markov_check;

  iarchive >> markov_check;

  const std::unordered_map<std::string, size_t> row_1 = markov.row("A");
  const std::unordered_map<std::string, size_t> row_1_check = markov_check.row("A");

  EXPECT_EQ(row_1.at("A"), row_1_check.at("A"));
  EXPECT_EQ(row_1.at("B"), row_1_check.at("B"));

  const std::unordered_map<std::string, size_t> row_2 = markov.row("B");
  const std::unordered_map<std::string, size_t> row_2_check = markov_check.row("B");

  EXPECT_EQ(row_2.at("A"), row_2_check.at("A"));
  EXPECT_EQ(row_2.at("A"), row_2_check.at("A"));
}


int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
  return 0;
}
