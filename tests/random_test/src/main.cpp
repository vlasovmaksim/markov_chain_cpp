#include "gtest/gtest.h"

#include <chrono>
#include <random>
#include <unordered_map>

TEST(GTEST, test1) {
  EXPECT_EQ(1, 1);
}

TEST(GTEST, test_generate_random_number_1) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

  std::default_random_engine generator(seed);

  std::discrete_distribution<int> distributor{ 0.5, 1 };

  int value = distributor(generator);

  EXPECT_GE(value, 0);
  EXPECT_LE(value, 1);
}

TEST(GTEST, test_generate_random_number_2) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

  std::default_random_engine generator(seed);

  std::discrete_distribution<int> distributor{ 1 };

  int value = distributor(generator);

  EXPECT_EQ(value, 0);
}

TEST(GTEST, test_generate_random_number_3) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

  std::default_random_engine generator(seed);

  std::discrete_distribution<int> distributor{ 0.1, 0.1, 0.1, 0.1, 0.1 };

  int value = distributor(generator);

  EXPECT_GE(value, 0);
  EXPECT_LE(value, 4);
}

TEST(GTEST, test_generate_random_number_4) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

  std::default_random_engine generator(seed);

  std::unordered_map<std::string, size_t> map = { { "A", 0 }, { "B", 1 } };

  std::vector<size_t> matrix;

  matrix.resize(map.size());

  size_t i = 0;

  for (const auto & element : map) {
    matrix[i] = element.second;
    i++;
  }

  std::discrete_distribution<size_t> distributor{ begin(matrix), end(matrix) };

  size_t value = distributor(generator);

  std::cerr << "value = " << value << std::endl;

  size_t j = 0;

  std::string result = "";

  for (const auto & element : map) {
    if (j == value) {
      result = element.first;

      break;
    }

    j++;
  }

  std::cerr << "result = " << result << std::endl;

  EXPECT_EQ(result, "B");
}

TEST(GTEST, test_generate_random_number_5) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

  std::default_random_engine generator(seed);

  std::unordered_map<std::string, size_t> map = { { "A", 1 }, { "B", 0 } };

  std::vector<size_t> matrix;

  matrix.resize(map.size());

  size_t i = 0;

  for (const auto & element : map) {
    matrix[i] = element.second;
    i++;
  }

  std::discrete_distribution<size_t> distributor{ begin(matrix), end(matrix) };

  size_t value = distributor(generator);

  std::cerr << "value = " << value << std::endl;

  size_t j = 0;

  std::string result = "";

  for (const auto & element : map) {
    if (j == value) {
      result = element.first;

      break;
    }

    j++;
  }

  std::cerr << "result = " << result << std::endl;

  EXPECT_EQ(result, "A");
}

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
  return 0;
}
