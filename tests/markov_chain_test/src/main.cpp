#include "gtest/gtest.h"

#include <chrono>
#include <random>
#include <vector>
#include <string>
#include <iostream>

#include "markovchain.hpp"
#include "util.hpp"


using namespace markov;

TEST(GTEST, test_marcov_parse_data_1) {
  std::vector<std::string> input_data = { "A", "B", "A"};

  MarkovChain markov;
  markov.parse(input_data);
}

TEST(GTEST, test_marcov_parse_data_2) {
  std::vector<std::string> input_data = { "A", "B", "A", "A" };

  MarkovChain markov;
  markov.parse(input_data);
}

TEST(GTEST, test_marcov_parse_data_3) {
  std::vector<std::string> input_data = { "A", "B", "A"};

  MarkovChain markov;
  markov.parse(input_data);
  const std::unordered_map<std::string, size_t> row_1 = markov.row("A");

  EXPECT_TRUE(row_1.find("A") == end(row_1));
  EXPECT_FALSE(row_1.find("B") == end(row_1));

  EXPECT_EQ(row_1.at("B"), 1);

  const std::unordered_map<std::string, size_t> row_2 = markov.row("B");

  EXPECT_FALSE(row_2.find("A") == end(row_1));
  EXPECT_TRUE(row_2.find("B") == end(row_1));

  EXPECT_EQ(row_2.at("A"), 1);
}

TEST(GTEST, test_marcov_parse_data_4) {
  std::vector<std::string> input_data = { "A", "B", "A", "A"};

  MarkovChain markov;
  markov.parse(input_data);
  const std::unordered_map<std::string, size_t> row_1 = markov.row("A");

  EXPECT_FALSE(row_1.find("A") == end(row_1));
  EXPECT_FALSE(row_1.find("B") == end(row_1));

  EXPECT_EQ(row_1.at("A"), 1);
  EXPECT_EQ(row_1.at("B"), 1);

  const std::unordered_map<std::string, size_t> row_2 = markov.row("B");

  EXPECT_FALSE(row_2.find("A") == end(row_1));
  EXPECT_TRUE(row_2.find("B") == end(row_1));

  EXPECT_EQ(row_2.at("A"), 1);
}

TEST(GTEST, test_marcov_1) {
  std::vector<std::string> input_data = { "A", "B", "A", "A"};

  MarkovChain markov;
  markov.set_current_item("A");

  EXPECT_EQ(markov.current_item(), "A");
}

TEST(GTEST, test_marcov_2) {
  std::vector<std::string> input_data = { "A", "B", "A"};

  MarkovChain markov;
  markov.parse(input_data);

  markov.set_current_item("A");

  EXPECT_EQ(markov.current_item(), "A");

  EXPECT_TRUE(markov.move_next());

  EXPECT_EQ(markov.current_item(), "B");

  EXPECT_TRUE(markov.move_next());

  EXPECT_EQ(markov.current_item(), "A");
}

TEST(GTEST, test_marcov_3) {
  std::vector<std::string> input_data = { "A", "B", "A", "A"};

  MarkovChain markov;
  markov.parse(input_data);

  markov.set_current_item("B");

  EXPECT_EQ(markov.current_item(), "B");

  EXPECT_TRUE(markov.move_next());

  EXPECT_EQ(markov.current_item(), "A");

  EXPECT_TRUE(markov.move_next());

  EXPECT_TRUE((markov.current_item() == "A")
            || (markov.current_item() == "B"));
}

TEST(GTEST, test_marcov_4) {
  std::vector<std::string> input_data = { "B", "A"};

  MarkovChain markov;
  markov.parse(input_data);

  markov.set_current_item("A");

  EXPECT_EQ(markov.current_item(), "A");

  EXPECT_FALSE(markov.move_next());
}

TEST(GTEST, test_input_read_to_vector_1) {
  std::vector<std::string> tmp = Util::read_to_vector(
        "test_input_read_to_vector_1.txt");

  EXPECT_EQ(tmp.size(), 10);
  EXPECT_EQ(tmp[0], "A");
  EXPECT_EQ(tmp[9], "D");
}

TEST(GTEST, test_marcov_ostream_1) {
  std::vector<std::string> input_data = { "B", "A"};

  MarkovChain markov;
  markov.parse(input_data);

  std::cout << markov;
}

TEST(GTEST, test_marcov_ostream_2) {
  std::vector<std::string> input_data = { "A", "B", "A", "A"};

  MarkovChain markov;
  markov.parse(input_data);

  std::cout << markov;
}

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
  return 0;
}
